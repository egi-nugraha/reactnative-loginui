import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';

export default class Logo extends React.Component{
    render(){
        return (
            <View style={styles.container}>
                <Image style={{width:100, height: 100}}
                    source={require('../images/login.png')}
                />
                <Text style={styles.logoText}>Welcome to My app.</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginVertical: 50,
        marginBottom: 65
    },
    logoText : {
        marginVertical: 15,
        fontSize: 18,
        color: "rgba(255,255,255, 0.7)"
    }
});