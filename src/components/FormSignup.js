import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';

export default class Form extends React.Component{
    render(){
        return (
            <View style={styles.container}>
                <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Email'
                    placeholderTextColor='#FFFFFF'
                />

                <TextInput secureTextEntry={true} style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Password'
                    placeholderTextColor='#FFFFFF'
                />

                <TextInput secureTextEntry={true} style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Confirm Password'
                    placeholderTextColor='#FFFFFF'
                />

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>{this.props.type}</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 350,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 5,
        paddingHorizontal: 18,
        fontSize: 16,
        color: '#FFFFFF',
        marginVertical: 10
    },
    button: {
        width:350,
        backgroundColor:'#1c313a',
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 14
    },
    buttonText: {
        fontSize:18,
        fontWeight: '500',
        color: '#FFFFFF',
        textAlign: 'center'
    }
});